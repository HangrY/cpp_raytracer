#include "Color.h"

namespace raytracer {

  Color::Color()
    : red(0), green(0), blue(0)
  {}
  
  Color::Color(unsigned char r, unsigned char g, unsigned char b)
    : red(r), green(g), blue(b)
  {}

  Color::Color(const Color &c)
    : red(c.red), green(c.green), blue(c.blue)
  {}

  void	Color::getColor(unsigned char *r, unsigned char *g, unsigned char *b) const {
    *r = red;
    *g = green;
    *b = blue;
  }

  unsigned char	Color::getRed() const { return (red); }
  unsigned char Color::getGreen() const { return (green); }
  unsigned char Color::getBlue() const { return (blue); }

  void	Color::setColor(unsigned char r, unsigned char g, unsigned char b) {
    red = r;
    green = g;
    blue = b;
  }

  void	Color::setColor(const Color &c) {
    red = c.red;
    green = c.green;
    blue = c.blue;
  }

  void	Color::setRed(unsigned char r) { red = r; }
  void	Color::setGreen(unsigned char g) { green = g; }
  void  Color::setBlue(unsigned char b) { blue = b; }

  std::ostream &operator<<(std::ostream &str, const Color &c) {
    std::cout << "\tRED\tGREEN\tBLUE\n";
    std::cout << "\t" << (int) c.getRed() << "\t" << (int) c.getGreen() << "\t" << (int) c.getBlue();
    return (str);
  }
  
}
