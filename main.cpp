#include <iostream>

#include "float3.h"
#include "Color.h"

using namespace raytracer;

int		main(int argc, char *argv[]) {
  (void) argc;
  (void) argv;

  Color c1;
  std::cout << c1 << std::endl;
  
  std::cout << "Hello World!" << std::endl;
  return (0);
}
