#ifndef LIGHT_H_
#define LIGHT_H_

#include <iostream>

#include "Color.h"
#include "float3.h"

namespace raytracer {

  enum class lightType {
    point,
      far,
      ambiant
      };
  
  class			Light {
  public:
    Light(const Color&, lightType, const float3&, const float3&);

    Color		getColor() const;
    float3		getRay(const float&) const;
    
  private:
    const Color		color;
    const lightType	type;
    const float3	position;
    const float3	direction;
  };
  
}

#endif /* !LIGHT_H_ */
