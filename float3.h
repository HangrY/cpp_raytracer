#ifndef FLOAT3_H_
#define FLOAT3_H_

#include <iostream>

namespace raytracer {

  class		float3 {
  public:
    float		x;
    float		y;
    float		z;
    
    float3();
    float3(float, float, float);
    float3(float3 &);
    
    float3	&operator+=(const float3 &);
    float3	&operator-=(const float3 &);
    float3	&operator*=(const float3 &);
    float3	&operator/=(const float3 &);
  };
  
  float3 operator+(const float3&, const float3&);
  float3 operator+(float, const float3&);
  float3 operator+(const float3&, float);
  
  float3 operator-(const float3&, const float3&);
  float3 operator-(float, const float3&);
  float3 operator-(const float3&, float);
  
  float3 operator*(const float3&, const float3&);
  float3 operator*(float, const float3&);
  float3 operator*(const float3&, float);
  
  float3 operator/(const float3&, const float3&);
  float3 operator/(const float3&, float);
  
  std::ostream &operator<<(std::ostream &, const float3&);
}

#endif /* !FLOAT_H_ */
