#include "float3.h"

namespace raytracer {

  float3::float3() {
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
  }
  
  float3::float3(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
  }
  
  float3::float3(float3 &f) {
    x = f.x;
    y = f.y;
    z = f.z;
  }
  
  float3	&float3::operator+=(const float3 &f) {
    x += f.x;
    y += f.y;
    z += f.z;
    return (*this);
  }
  
  float3	operator+(const float3 &f1, const float3 &f2) {
    float3	res(f1.x + f2.x, f1.y + f2.y, f1.z + f2.z);
    return (res);
  }
  
  float3	operator+(float f1, const float3 &f2) {
    float3	res(f1 + f2.x, f1 + f2.y, f1 + f2.z);
    return (res);
  }
  
  float3	operator+(const float3 &f1, float f2) {
    float3	res(f1.x + f2, f1.y + f2, f1.z + f2);
    return (res);
  }

  float3	&float3::operator-=(const float3 &f) {
    x -= f.x;
    y -= f.y;
    z -= f.z;
    return (*this);
  }
  
  float3	operator-(const float3 &f1, const float3 &f2) {
    float3	res(f1.x - f2.x, f1.y - f2.y, f1.z - f2.z);
    return (res);
  }
  
  float3	operator-(float f1, const float3 &f2) {
    float3	res(f1 - f2.x, f1 - f2.y, f1 - f2.z);
    return (res);
  }
  
  float3	operator-(const float3 &f1, float f2) {
    float3	res(f1.x - f2, f1.y - f2, f1.z - f2);
    return (res);
  }

  float3	&float3::operator*=(const float3 &f) {
    x *= f.x;
    y *= f.y;
    z *= f.z;
    return (*this);
  }
  
  float3	operator*(const float3 &f1, const float3 &f2) {
    float3	res(f1.x * f2.x, f1.y * f2.y, f1.z * f2.z);
    return (res);
  }
  
  float3	operator*(float f1, const float3 &f2) {
    float3	res(f1 * f2.x, f1 * f2.y, f1 * f2.z);
    return (res);
  }
  
  float3	operator*(const float3 &f1, float f2) {
    float3	res(f1.x * f2, f1.y * f2, f1.z * f2);
    return (res);
  }

  float3	&float3::operator/=(const float3 &f) {
    x /= f.x;
    y /= f.y;
    z /= f.z;
    return (*this);
  }
  
  float3	operator/(const float3 &f1, const float3 &f2) {
    float3	res(f1.x / f2.x, f1.y / f2.y, f1.z / f2.z);
    return (res);
  }
  
  float3	operator/(const float3 &f1, float f2) {
    float3	res(f1.x / f2, f1.y / f2, f1.z / f2);
    return (res);
  }
  
  std::ostream &operator<<(std::ostream &str, const float3 &f) {
    std::cout << "\n  x : " << f.x << "\n  y : " << f.y << "\n  z : " << f.z;
    return (str);
  }
 
}
