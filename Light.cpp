#include "Light.h"

namespace raytracer {

  Light::Light(const Color &c, lightTypre type, const float3 &pos, const float3 &dir)
    : color(c), type(type), position(pos), direction(dir)
  {}

  Color		Light::getColor() const { return (color); ]

  float3	Light::getRay(const float3 &point) const {
    return (position - point);
  }

}
