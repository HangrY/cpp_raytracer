
CXX= g++

CXXFLAGS= -W -Wall -ansi -pedantic -g

LDFLAGS=

RAYTRACER= raytracer

RAYTRACER_SRC=	float3.cpp	\
		Color.cpp	\
		main.cpp

RAYTRACER_OBJ= $(RAYTRACER_SRC:.cpp=.o)

all: $(RAYTRACER)

${RAYTRACER}: $(RAYTRACER_OBJ)
	$(CXX) -o $@ $(RAYTRACER_OBJ) $(LDFLAGS)

clean:
	rm -rf $(RAYTRACER_OBJ)

fclean: clean
	rm -rf $(RAYTRACER)

re: fclean all

