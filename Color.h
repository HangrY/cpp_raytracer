#ifndef COLOR_H_
#define COLOR_H_

#include <iostream>

namespace raytracer {

  class			Color {
  public:
    Color();
    Color(unsigned char, unsigned char, unsigned char);
    Color(const Color&);

    void		getColor(unsigned char *, unsigned char *, unsigned char *) const;
    unsigned char	getRed() const;
    unsigned char	getGreen() const;
    unsigned char	getBlue() const;

    void		setColor(unsigned char, unsigned char, unsigned char);
    void		setColor(const Color&);
    void		setRed(unsigned char);
    void		setGreen(unsigned char);
    void		setBlue(unsigned char);

    /*	TODO
    ** function to darken or lighten
    ** function to mix colors
    */
    
  private:
    unsigned char	red;
    unsigned char	green;
    unsigned char	blue;
  };

  std::ostream &operator<<(std::ostream &, const Color&);
}

#endif /* !COLOR_H_ */
